from main import Game

game = Game()
game.show_start_screen()
while game.running:
    game.new()
    game.show_end_screen()
