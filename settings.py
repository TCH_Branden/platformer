TITLE = 'Tower Rise'
WIDTH  = 500
HEIGHT = 800
FPS = 60
FONT_NAME = 'arial'
HS_FILE = 'highscore.txt'
SPRITESHEET = 'spritesheet.png'
TILESET = 'tileset.png'

PLAYER_ACC = 0.6
PLAYER_FRICTION = -0.10
PLAYER_JUMP = -11
GRAVITY = 0.4

WHITE = (255, 255, 255)
BLACK = (30, 29, 22)
COLOR_KEY = (0, 0, 0)
RED   = (192, 50 ,40)
GREEN = (35, 76, 60)
BLUE  = (0, 33, 84)
YELLOW  = (227, 233, 58)
HERO  = (255, 255, 0)
DARK_GREY = (119, 119, 119)
BROWN = (60, 40, 14)

# Hero Properties
X = 30
Y = 30

# Starting platforms
PLATFORM_LIST = [
    (0, HEIGHT - 81, 'base'),
    (337, HEIGHT - 81, 'base'),
    (WIDTH /2 - 50, HEIGHT * 3/4 - 10, 'base_plat'),
    (WIDTH - 100, HEIGHT * 2/4, 'base_plat'),
    (100, HEIGHT * 1/4, 'base_plat'),
]
WALLS = [
    (0, -800, 'wall_l'),
    (0, -400, 'wall_l'),
    (0, 1, 'wall_l'),
    (0, 400, 'wall_l'),
    (0, 400*2, 'wall_l'),
    (WIDTH - 23, -800, 'wall_r'),
    (WIDTH - 23, -400, 'wall_r'),
    (WIDTH - 23, 1, 'wall_r'),
    (WIDTH - 23, 400, 'wall_r'),
    (WIDTH - 23, 400*2, 'wall_r'),
]

TILES = [
    (WIDTH / 2 - 40, HEIGHT - 161, 'door'),
    (WIDTH / 2 - 150, HEIGHT - 200, 'dark_brick'),
    (WIDTH / 3 - 40, HEIGHT - 300, 'dark_brick'),
    (WIDTH / 3 - 100, HEIGHT - 350, 'dark_brick'),
    (WIDTH / 3 - 40, HEIGHT - 500, 'dark_brick'),
    (WIDTH / 2 + 80, HEIGHT - 600, 'dark_brick'),
    (WIDTH / 2 + 80, HEIGHT - 600, 'dark_brick'),
    (WIDTH / 2 + 80, HEIGHT - 600, 'dark_brick'),
    (WIDTH / 2 + 80, HEIGHT - 600, 'dark_brick'),
]

# Class settings
# Knight
KNIGHT_JUMPS = 2
