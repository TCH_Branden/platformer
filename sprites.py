import pygame as pg
import random
from settings import *
vector = pg.math.Vector2

class Player(pg.sprite.Sprite):
    def __init__(self, game):
        pg.sprite.Sprite.__init__(self)
        # Initialize various player properties.
        # Player physics values.
        self.game = game
        self.total_jumps = 2
        self.jumps = 0
        self.total_dashes = 1
        self.dashes = 0
        self.wall_hit = False
        self.wall_jump = True

        # Player animation triggers.
        self._layer = 1
        self.walking = False
        self.walking_l = False
        self.walking_r = True
        self.dashing = False
        self.jumping = False
        self.falling = False
        self.double_jumping = False
        self.wallsliding = False
        self.idle = False
        self.crouching = False
        self.current_frame = 0
        self.last_update = 0

        self.load_images()
        self.image = self.game.spritesheet.get_image(14, 6, 20, 30)

        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, HEIGHT / 2)
        self.pos = vector(WIDTH / 2, HEIGHT / 2)
        self.vel = vector(0, 0)
        self.acc = vector(0, 0)

    def load_images(self):
        self.standing_r_frames = [
            self.game.spritesheet.get_image(14, 6, 20, 30),
            self.game.spritesheet.get_image(14, 6, 20, 30),
            self.game.spritesheet.get_image(14, 6, 20, 30),
            self.game.spritesheet.get_image(64, 6, 20, 30),
            self.game.spritesheet.get_image(64, 6, 20, 30),
            self.game.spritesheet.get_image(64, 6, 20, 30),
            self.game.spritesheet.get_image(114, 6, 20, 30),
            self.game.spritesheet.get_image(114, 6, 20, 30),
            self.game.spritesheet.get_image(114, 6, 20, 30),
            self.game.spritesheet.get_image(164, 6, 20, 30),
            self.game.spritesheet.get_image(164, 6, 20, 30),
            self.game.spritesheet.get_image(164, 6, 20, 30),
        ]
        for frame in self.standing_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.standing_l_frames = []
        for frame in self.standing_r_frames:
            self.standing_l_frames.append(pg.transform.flip(frame, True, False))

        self.jumping_r_frames = [
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(116, 78, 20, 30),
            self.game.spritesheet.get_image(213, 78, 20, 30),
            self.game.spritesheet.get_image(213, 78, 20, 30),
            self.game.spritesheet.get_image(213, 78, 20, 30),
            self.game.spritesheet.get_image(213, 78, 20, 30),
            self.game.spritesheet.get_image(213, 78, 20, 30),
        ]
        for frame in self.jumping_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.jumping_l_frames = []
        for frame in self.jumping_r_frames:
            self.jumping_l_frames.append(pg.transform.flip(frame, True, False))

        self.d_jumping_r_frames = [
            self.game.spritesheet.get_image(268, 84, 20, 30),
            self.game.spritesheet.get_image(318, 84, 20, 30),
            self.game.spritesheet.get_image(14, 120, 20, 30),
        ]
        for frame in self.d_jumping_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.d_jumping_l_frames = []
        for frame in self.d_jumping_r_frames:
            self.d_jumping_l_frames.append(pg.transform.flip(frame, True, False))

        self.falling_r_frames = [
            self.game.spritesheet.get_image(65, 113, 20, 30),
            self.game.spritesheet.get_image(65, 113, 20, 30),
            self.game.spritesheet.get_image(65, 113, 20, 30),
            self.game.spritesheet.get_image(115, 113, 20, 30),
            self.game.spritesheet.get_image(115, 113, 20, 30),
            self.game.spritesheet.get_image(115, 113, 20, 30),
        ]
        for frame in self.falling_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.falling_l_frames = []
        for frame in self.falling_r_frames:
            self.falling_l_frames.append(pg.transform.flip(frame, True, False))

        self.walking_r_frames = [
            self.game.spritesheet.get_image(67, 43, 20, 30),
            self.game.spritesheet.get_image(116, 43, 20, 30),
            self.game.spritesheet.get_image(217, 43, 20, 30),
            self.game.spritesheet.get_image(266, 43, 20, 30),
            self.game.spritesheet.get_image(216, 43, 20, 30),
        ]
        for frame in self.walking_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.walking_l_frames = []
        for frame in self.walking_r_frames:
            self.walking_l_frames.append(pg.transform.flip(frame, True, False))

        self.crouching_r_frames = [
            self.game.spritesheet.get_image(215, 6, 20, 30),
            self.game.spritesheet.get_image(215, 6, 20, 30),
            self.game.spritesheet.get_image(215, 6, 20, 30),
            self.game.spritesheet.get_image(215, 6, 20, 30),
            self.game.spritesheet.get_image(215, 6, 20, 30),
            self.game.spritesheet.get_image(215, 6, 20, 30),
            self.game.spritesheet.get_image(265, 6, 20, 30),
            self.game.spritesheet.get_image(265, 6, 20, 30),
            self.game.spritesheet.get_image(265, 6, 20, 30),
            self.game.spritesheet.get_image(265, 6, 20, 30),
            self.game.spritesheet.get_image(265, 6, 20, 30),
            self.game.spritesheet.get_image(265, 6, 20, 30),
        ]
        for frame in self.crouching_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.crouching_l_frames = []
        for frame in self.crouching_r_frames:
            self.crouching_l_frames.append(pg.transform.flip(frame, True, False))

        self.dashing_r_frames = [
            self.game.spritesheet.get_image(155, 139, 38, 14),
        ]
        for frame in self.dashing_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.dashing_l_frames = []
        for frame in self.dashing_r_frames:
            self.dashing_l_frames.append(pg.transform.flip(frame, True, False))

        self.wallslide_r_frames = [
            self.game.spritesheet.get_image(113, 411, 20, 30),
            self.game.spritesheet.get_image(162, 410, 20, 30),
        ]
        for frame in self.wallslide_r_frames:
            frame.set_colorkey(COLOR_KEY)

        self.wallslide_l_frames = []
        for frame in self.wallslide_r_frames:
            self.wallslide_l_frames.append(pg.transform.flip(frame, True, False))

    # Player update function
    def update(self):
        self.animate()
        # Set acceleration
        self.acc = vector(0, GRAVITY)
        keys = pg.key.get_pressed()

        # Move charater based on key press.
        if keys[pg.K_LEFT]:
            self.acc.x = -PLAYER_ACC
            if not self.jumping:
                self.walking = True
            self.walking_l = True
            self.walking_r = False
        elif keys[pg.K_RIGHT]:
            self.acc.x = PLAYER_ACC
            if not self.jumping:
                self.walking = True
            self.walking_l = False
            self.walking_r = True
        else:
            self.walking = False

        if keys[pg.K_DOWN]:
            self.vel.x = 0
            self.crouching = True
        else:
            self.crouching = False


        # Motion / Running
        self.acc.x += self.vel.x * PLAYER_FRICTION
        self.vel += self.acc

        if abs(self.vel.x) < 0.1:
            self.vel.x = 0

        self.pos += self.vel + PLAYER_ACC * self.acc

        # Screen wrap logic. Currently the walls prevent
        if self.pos.x > WIDTH: self.pos.x = 0
        if self.pos.x < 0: self.pos.x = WIDTH

        # Update Position
        self.rect.midbottom = self.pos

        if self.vel.y > 0:
            self.jumping = False
            self.double_jumping = False
            self.falling = True
        elif self.vel.y < 0:
            self.falling = False

        if self.wallsliding:
            self.vel.y = self.vel.y / 4 * 3


    def animate(self):
        # TODO: Clean this the hell up.
        now = pg.time.get_ticks()
        if not self.jumping and not self.walking and not self.crouching and not self.falling:
            if now - self.last_update > FPS * 2:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.standing_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.standing_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.standing_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
        elif self.crouching and not self.wallsliding:
            if now - self.last_update > FPS:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.crouching_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.crouching_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.crouching_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
        elif self.walking and not self.jumping and not self.wallsliding:
            if now - self.last_update > FPS * 2:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.walking_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.walking_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.walking_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
        elif self.jumping and not self.falling and not self.double_jumping and not self.wallsliding:
            if now - self.last_update > FPS:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.jumping_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.jumping_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.jumping_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
        elif self.jumping and self.double_jumping and not self.falling and not self.wallsliding:
            if now - self.last_update > FPS:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.d_jumping_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.d_jumping_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.d_jumping_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
        elif self.falling and not self.jumping and not self.dashing and not self.double_jumping and not self.wallsliding:
            if now - self.last_update > FPS:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.falling_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.falling_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.falling_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
        elif self.wallsliding:
            if now - self.last_update > FPS:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.wallslide_r_frames)
                bottom = self.rect.bottom
                if self.walking_l:
                    new_frame = self.wallslide_l_frames[self.current_frame]
                elif self.walking_r:
                    new_frame = self.wallslide_r_frames[self.current_frame]
                self.image = new_frame
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom

    def jump(self):
        # If the player is grounded jump, else allow for multi jump based on class setting.
        self.rect.y += 1
        platform_collision = pg.sprite.spritecollide(self, self.game.platforms, False)
        self.rect.y -= 1

        # If standing on a platform.
        if platform_collision:
            # Change the players y velocity to cause a jump.
            self.vel.y = PLAYER_JUMP
            self.jumps += 1

            # if self.crouching == True:
            #     self.vel.y - 30

            self.jumping = True
            self.crouching = False
            self.walking = False
        # Else this is an air jump.
        elif self.vel.y != 0:
            # The first time the player comes in contact with a wall while airborn
            # they are able to wall jump. This doesn't mean they do however. first
            # we have to check if they are still "on" the wall before we allow a wall jump.
            if self.wall_hit and self.wall_jump:
                # Diable future wall jumps until the player lands on a platform agian.
                self.wall_jump = False

                keys = pg.key.get_pressed()

                # The next two checks are for jumping off each wall.
                # I use a similar method to checking if the players on a platform.
                # I use 3 px leeway to make the wall jump easier but not free.
                if keys[pg.K_LEFT]:
                    self.rect.x += 3
                    wall_collision = pg.sprite.spritecollide(self, self.game.walls, False)
                    self.rect.x -= 3

                    if wall_collision:
                        self.vel.x -= 8
                        self.vel.y = PLAYER_JUMP - 4
                        self.jumping = True
                        self.double_jumping = False

                if keys[pg.K_RIGHT]:
                    self.rect.x -= 3
                    wall_collision = pg.sprite.spritecollide(self, self.game.walls, False)
                    self.rect.x += 3

                    if wall_collision:
                        self.vel.x += 8
                        self.vel.y = PLAYER_JUMP - 4
                        self.jumping = True
                        self.double_jumping = False

            # Else double jump.
            elif self.jumps < self.total_jumps:
                self.vel.y = PLAYER_JUMP
                self.jumps += 1
                self.double_jumping = True
                self.jumping = True
                self.falling = False

    def dash(self, direction):
        # Allows the player to dash in the x directions. pretty self explainitory.
        if self.dashes < self.total_dashes:
            if direction == 'up':
                self.vel.x = 0
                self.vel.y -= 7
                self.dashes += 1
            else:
                if self.vel.x > 0:
                    self.vel.x += 23
                    self.dashes += 1

                if self.vel.x < 0:
                    self.vel.x -= 23
                    self.dashes += 1

# Platform sprite class for platforms and walls.
class Platform(pg.sprite.Sprite):
    def __init__(self, x, y, type, game):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.load_images()

        if type == 'plat':
            plat = random.randrange(0, 3)
            self.image = self.platform_frames[plat]
        else:
            self.image = self.base_frames[type]

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def load_images(self):
        self.base_frames = {
            'base': self.game.tileset.get_image(47, 62, 337, 81),
            'base_plat': self.game.tileset.get_image(47, 62, 100, 20),
            'wall_l': self.game.tileset.get_image(0, 0, 23, 500),
            'wall_r': self.game.tileset.get_image(23, 0, 23, 500),
        }
        for frame in self.base_frames:
            self.base_frames[frame].set_colorkey(COLOR_KEY)

        self.platform_frames = [
            self.game.tileset.get_image(94, 0, 32, 14),
            self.game.tileset.get_image(94, 15, 62, 14),
            self.game.tileset.get_image(94, 30, 90, 14),
        ]

    def update(self):
        pass
        # self.image.fill(self.game.platform_color)

class Tile(pg.sprite.Sprite):
    def __init__(self, x, y, type, game):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.load_images()
        if type == 'dark_brick':
            num = random.randrange(0, 3)
            if num == 2:
                if random.random() > .1:
                    num = num = random.randrange(0, 2)

            self.image = self.brick_frames[num]
        else:
            self.image = self.tile_frames[type]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def load_images(self):
        self.brick_frames = [
            self.game.tileset.get_image(46, 30, 16, 15),
            self.game.tileset.get_image(78, 30, 16, 15),
            # self.game.tileset.get_image(46, 30, 16, 30),
            # self.game.tileset.get_image(46, 45, 30, 15),
            self.game.tileset.get_image(80, 245, 32, 48),
        ]

        for frame in self.brick_frames:
            frame.set_colorkey(COLOR_KEY)

        self.tile_frames = {
            'door': self.game.tileset.get_image(192, 213, 80, 80),
        }
        for frame in self.tile_frames:
            self.tile_frames[frame].set_colorkey(COLOR_KEY)

    def update(self):
        pass

class Spritesheet:
    # Utility class for loading and parsing spritesheets.
    def __init__(self, filename):
        self.spritesheet = pg.image.load(filename).convert()

    def get_image(self, x, y, w, h):
        # Grab an image out of a larger spritesheet.
        image = pg.Surface((w, h))
        image.blit(self.spritesheet, (0, 0), (x, y, w ,h))
        return image
