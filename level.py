import pygame as pg
import random
from settings import *
from sprites import Platform, Tile

class LevelManager:

    def __init__(self, platform_total, platform_type, game):
        # Init level
        self.game = game
        self.platform_total = platform_total
        self.tile_total = 9
        self.platform_type = platform_type
        self.score = 0
        self.high_score = 0

    def update(self):
        if self.score >= 300:
            self.platform_total = 6
            self.platform_type = 'base_plat'

        if self.score >= 600:
            self.platform_total = 5
            self.platform_type = 'plat'

        if self.score >= 900:
            self.platform_total = 4
            self.platform_type = 'plat'

        if self.score >= 1200:
            self.platform_total = 3
            self.platform_type = 'base_plat'

        if self.score >= 1500:
            self.platform_total = 2
            self.platform_type = 'base_plat'

        # Generate random platforms as the player moves up the level.
        while len(self.game.platforms) < self.platform_total:
            while True:
                p = Platform(random.randrange(23, WIDTH - 180),
                             random.randrange(-90, -28),
                             self.platform_type,
                             self.game)
                sprite_collision = pg.sprite.spritecollide(p, self.game.all_sprites, False)
                if not sprite_collision:
                    break
            self.game.platforms.add(p)
            self.game.all_sprites.add(p)

        # Generate random platforms as the player moves up the level.
        while len(self.game.tiles) < self.tile_total:
            while True:
                t = Tile(random.randrange(23, WIDTH - 39),
                             random.randrange(-200, -28),
                             'dark_brick',
                             self.game)
                sprite_collision = pg.sprite.spritecollide(t, self.game.all_sprites, False)
                if not sprite_collision:
                    break
            self.game.tiles.add(t)
            self.game.all_sprites.add(t)


        # Create new walls.
        while len(self.game.walls) <= 10:
            new_walls = [
            (0, -700, 'wall_l'),
            (0, -1100, 'wall_l'),
            (WIDTH - 23, -700, 'wall_r'),
            (WIDTH - 23, -1100, 'wall_r'),
            ]

            for wall in new_walls:
                w = Platform(*wall, self.game)
                self.game.walls.add(w)
                self.game.all_sprites.add(w)




    def new(self):
        self.reset()
        # Create inital platforms and add them to sprite list.
        for plat in PLATFORM_LIST:
            p = Platform(*plat, self.game)
            self.game.platforms.add(p)
            self.game.all_sprites.add(p)
        # Do the same for the walls.
        # Walls are in a seperate list because of the kill routine in update() for platforms.
        for wall in WALLS:
            w = Platform(*wall, self.game)
            self.game.walls.add(w)
            self.game.all_sprites.add(w)

        for tile in TILES:
            t = Tile(*tile, self.game)
            self.game.tiles.add(t)
            self.game.all_sprites.add(t)

    def reset(self):
        self.score = 0
        self.platform_total = 6
        self.platform_type = 'base_plat'
