import pygame as pg
import random
import os
from os import path
import time

from utils import *
from settings import *
from sprites import *
from level import *

class Game:

    def __init__(self):
        # Init pygame
        pg.init()
        pg.mixer.init()
        # Create the screen
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        pg.display.set_caption(TITLE)
        # Game loop variables
        self.clock = pg.time.Clock()
        self.running = True

        # Set game level. This controls the amount of platforms allowed on screen
        # at one time. The higher the number, the easier the game becomes.
        self.level = LevelManager(6, 'base_plat', self)

        self.load_data()

    # load_data is used to load static assets.
    def load_data(self):
        # load the high score.
        self.dir = path.dirname(__file__)
        img_dir = path.join(self.dir, 'img')
        with open(path.join(self.dir, HS_FILE), 'r') as f:
            try:
                self.level.high_score = int(f.read())
            except Exception as e:
                print(e)
                self.level.high_score = 0

        self.spritesheet = Spritesheet(path.join(img_dir, SPRITESHEET))
        self.tileset = Spritesheet(path.join(img_dir, TILESET))

    # Create a new game.
    def new(self):
        # Make sprite groups, this will allow us to manage sprites in a list.
        self.all_sprites = pg.sprite.LayeredUpdates()
        # TODO: Move these sprite groups to LevelManager.
        self.platforms = pg.sprite.Group()
        self.walls = pg.sprite.Group()
        self.tiles = pg.sprite.Group()
        # Create player and add it to the sprite list.
        self.player = Player(self)
        self.all_sprites.add(self.player)
        self.level.new()
        self.run()

    # Main game loop.
    def run(self):
        self.playing = True
        while self.playing:
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()

    # Catures game events, used for menuing and sprite function like jump, etc..
    # Logic for button combos and player attribute manipulation (Wall/Double jump for example)
    # are kept in the player update() to keep this clean.
    def events(self):
        # Capture events.
        for event in pygame.event.get():
            # Quit event.
            if event.type == pygame.QUIT:
                self.playing = False
                self.running = False

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_z:
                    self.player.jump()

                if event.key == pg.K_x:
                    self.player.dash('side')

    # Game world update checks for various game states such as platform and wall
    # collision with player, pushing the screen up and the player climbs, generating
    # new / removing old platforms, and handling game over checks.
    def update(self):
        self.all_sprites.update()
        self.check_collisions()
        self.scroll_screen()
        self.level.update()
        self.check_game_over()

    # Paints each frame to the screen.
    def draw(self):
        # Reset the scene.
        self.screen.fill(BLACK)
        self.all_sprites.draw(self.screen)
        self.draw_text("%s" % str(self.level.score), 32, YELLOW if self.level.score > self.level.high_score else WHITE, 60, 20)
        pg.display.flip()

    # Detects various collisions and updates game/player state.
    def check_collisions(self):
        # Check if player hits a playform, only if falling
        if self.player.vel.y > 0:
            platform_collision = pg.sprite.spritecollide(self.player, self.platforms, False)
            if platform_collision:
                # Stop the player.
                self.player.vel.y = 0
                self.player.falling = False

                # Place them on top of the platform.
                self.player.pos.y = platform_collision[0].rect.top + 1
                # Reset movement variables
                self.player.jumps = 0
                self.player.wall_jumps = 0
                self.player.dashes = 0
                self.player.wall_hit = False
                self.player.wall_jump = True
                self.player.jumping = False
                self.player.double_jumping = False

        # Detect if player collides with wall.
        wall_collision = pg.sprite.spritecollide(self.player, self.walls, False)
        if wall_collision:
            if self.player.vel.x > 0:
                # Remove a jump counter for wall jump
                self.player.wall_hit = True
                self.player.vel.x = 0
                self.player.pos.x = wall_collision[0].rect.left - 9
                self.player.wallsliding = True

            if self.player.vel.x < 0:
                self.player.wall_hit = True
                self.player.vel.x = 0
                self.player.pos.x = wall_collision[0].rect.right + 9.9
                self.player.wallsliding = True
        else:
            self.player.wallsliding = False

    # Scrolls the screen up and the player jumps, kills platforms, awards points.
    def scroll_screen(self):
        # If player reaches top 4th of the screen.
        if self.player.rect.top <= HEIGHT / 3:
            # Lock the players position to the players velocity "scrolling"
            # the screen.
            self.player.pos.y += abs(self.player.vel.y)
            # If a platform goes off screen, remove it. Award points.
            for plat in self.platforms:
                plat.rect.top += abs(self.player.vel.y)
                if plat.rect.top >= HEIGHT:
                    plat.kill()
                    self.level.score += 10

            for wall in self.walls:
                wall.rect.top += abs(self.player.vel.y)
                if wall.rect.top >= HEIGHT:
                    wall.kill()

            for tile in self.tiles:
                tile.rect.top += abs(self.player.vel.y)
                if tile.rect.top >= HEIGHT:
                    tile.kill()

    # Checks for gameover state and end the game.
    def check_game_over(self):
        if self.player.rect.bottom > HEIGHT:
            for p in self.platforms:
                p.rect.y -= max(self.player.vel.y, 10)
                if p.rect.bottom < 0:
                    p.kill()

            for w in self.walls:
                w.rect.y -= max(self.player.vel.y, 10)
                if w.rect.bottom < 0:
                    w.kill()

            for t in self.tiles:
                t.rect.y -= max(self.player.vel.y, 10)
                if t.rect.bottom < 0:
                    t.kill()

            if len(self.platforms) == 0:
                self.playing = False

    # Utility function to help draw text.
    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font('alagard.ttf', size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)

    # Builds and displays the start screen, waits for player input.
    def show_start_screen(self):
        self.screen.fill(BLACK)
        self.title_image = get_image('img/title.png')
        self.screen.blit(self.title_image, (0, HEIGHT / 5))
        self.draw_text("Press Any Key to Start.", 25, RED, WIDTH / 2, HEIGHT / 2 + 50)
        self.draw_text('High Score: %s' % str(self.level.high_score), 35, RED, WIDTH / 2, HEIGHT - 100)
        pg.display.flip()
        self.wait_for_key()

    # Builds and shows the end screen, game over.
    def show_end_screen(self):
        if not self.running:
            return
        self.screen.fill(BLACK)
        self.end_image = get_image('img/end_title.png')
        self.screen.blit(self.end_image, (0, 50))
        self.draw_text("Final Score: %s" % str(self.level.score), 35, RED, WIDTH / 2, HEIGHT / 2)
        self.draw_text("Press Any Key to Play Again.", 25, RED, WIDTH / 2, HEIGHT / 2 + 30)

        # If a new high score is set, save it to the highscore.txt file
        if self.level.score > self.level.high_score:
            self.draw_text("New High Score! ", 35, RED, WIDTH / 2, HEIGHT - 100)
            self.level.high_score = self.level.score
            with open(path.join(self.dir, HS_FILE), 'w') as f:
                try:
                    f.write(str(self.level.score))
                except Exception as e:
                    print(e)
        else:
            self.draw_text("High Score: %s" % self.level.high_score, 35, RED, WIDTH / 2, HEIGHT - 100)

        pg.display.flip()
        self.wait_for_key()


    # Waits for player input to proceed.
    def wait_for_key(self):
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for events in pg.event.get():
                if events.type == pg.QUIT:
                    waiting = False
                    self.running = False
                if events.type == pg.KEYUP:
                    waiting = False
